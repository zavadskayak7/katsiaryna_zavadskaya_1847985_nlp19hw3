from utils import *
import keras


def predict_babelnet(input_path: str, output_path: str, resources_path: str) -> None:
    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <BABELSynset>" format (e.g. "d000.s000.t000 bn:01234567n").

    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.

    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """
    # importing trained model
    model = keras.models.load_model(resources_path + '/model--07.hd5f')
    # parsing of test set
    tokens_test, labels_test, POS_test, _, synsetmapping_test = xml_parsing(
        input_path)
    # loading of dictionaries and masks
    tokens_dict = load_dict('tokens', resources_path)
    pos_dict = load_dict('pos', resources_path)
    babelnet_labels_dict = load_dict('babelnet_labels', resources_path)
    lex_labels_dict = load_dict('lex_labels', resources_path)
    mask_senses = load_dict('mask_senses', resources_path)
    mask_lex = load_dict('mask_lex', resources_path)
    bn_to_wn_dict = load_dict('bn_to_wn_dict', resources_path)

    #recover hyperparameters
    BATCH_SIZE = 64
    SEQ_LEN = 80
    OUTPUT_VOCAB_SIZE = len(list(babelnet_labels_dict.keys()))
    LEX_OUTPUT_VOCAB_SIZE = len(list(lex_labels_dict.keys()))
    input_words_test = applyDict(tokens_test, tokens_dict)
    input_pos_test = applyDict(POS_test, pos_dict)


    X_test = keras.preprocessing.sequence.pad_sequences(
        input_words_test, maxlen=SEQ_LEN, padding='post')
    X_pos_test = keras.preprocessing.sequence.pad_sequences(
        input_pos_test, maxlen=SEQ_LEN, padding='post')
    # batch generator for test set
    test_generator = dataGenerator(X_test, [], X_pos_test, [],
                                   OUTPUT_VOCAB_SIZE, LEX_OUTPUT_VOCAB_SIZE,
                                   BATCH_SIZE, mask_senses, mask_lex, False)

    STEPS_VAL = int(np.ceil(X_test.shape[0]/BATCH_SIZE))
    # getting arrays of predicions for test set
    output = model.predict_generator(
        test_generator, steps=STEPS_VAL, verbose=True)
    # fine-grained predictions for test set
    pred_max_fine = np.argmax(output[0], axis=-1)
    # mapping from digit to BN id, used to transform model predictions to BN
    map_label_bn = {v: k for k, v in babelnet_labels_dict.items()}
    predictions_sentences_bn = dict()
    # iterating over number of sentences in test set
    for i in range(len(labels_test)):
        # cutting padded prediction to the length of input sentence
        lbls_bn_pred = list(pred_max_fine[i])[0:len(labels_test[i])]
        # convert labels from digit to bn
        output_dict_bn = predictions_transformation(
            labels_test[i], lbls_bn_pred, POS_test[i], tokens_test[i], map_label_bn, bn_to_wn_dict)
        predictions_sentences_bn = dict(
            list(predictions_sentences_bn.items()) + list(output_dict_bn.items()))

    save_dict(predictions_sentences_bn,
              "predictions_sentences_bn", resources_path)
    #write on output file
    with open(output_path, mode='w') as f:
        keys = list(predictions_sentences_bn.keys())
        values = list(predictions_sentences_bn.values())
        for i in range(len(keys)):
            if i < len(keys) - 1:
                f.write(str(keys[i]) + ' ' + str(values[i]) + '\n')
            else:
                f.write(str(keys[i]) + ' ' + str(values[i]))
        print(output_path, ' is written')


def predict_wordnet_domains(input_path: str, output_path: str, resources_path: str) -> None:
    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <wordnetDomain>" format (e.g. "d000.s000.t000 sport").

    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.

    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """
    # restore neural network model
    model = keras.models.load_model(resources_path + '/model--07.hd5f')
    # parse input file
    tokens_test, labels_test, POS_test, _, synsetmapping_test = xml_parsing(
        input_path)
    # reload dictionaries from disk
    tokens_dict = load_dict('tokens', resources_path)
    pos_dict = load_dict('pos', resources_path)
    babelnet_labels_dict = load_dict('babelnet_labels', resources_path)
    lex_labels_dict = load_dict('lex_labels', resources_path)
    mask_senses = load_dict('mask_senses', resources_path)
    mask_lex = load_dict('mask_lex', resources_path)
    bn_to_wn_dict = load_dict('bn_to_wn_dict', resources_path)

    # recover hyperparameters
    BATCH_SIZE = 64
    SEQ_LEN = 80
    OUTPUT_VOCAB_SIZE = len(list(babelnet_labels_dict.keys()))
    LEX_OUTPUT_VOCAB_SIZE = len(list(lex_labels_dict.keys()))
    input_words_test = applyDict(tokens_test, tokens_dict)
    input_pos_test = applyDict(POS_test, pos_dict)
    # pad sentences
    X_test = keras.preprocessing.sequence.pad_sequences(
        input_words_test, maxlen=SEQ_LEN, padding='post')
    X_pos_test = keras.preprocessing.sequence.pad_sequences(
        input_pos_test, maxlen=SEQ_LEN, padding='post')
    # generator for predictions
    test_generator = dataGenerator(X_test, [], X_pos_test, [],
                                   OUTPUT_VOCAB_SIZE, LEX_OUTPUT_VOCAB_SIZE,
                                   BATCH_SIZE, mask_senses, mask_lex, False)

    # network prediction
    STEPS_VAL = int(np.ceil(X_test.shape[0]/BATCH_SIZE))
    output = model.predict_generator(
        test_generator, steps=STEPS_VAL, verbose=True)
    pred_max_fine = np.argmax(output[0], axis=-1)

    map_label_bn = {v: k for k, v in babelnet_labels_dict.items()}
    predictions_sentences_bn = dict()
    for i in range(len(labels_test)):
        lbls_bn_pred = list(pred_max_fine[i])[0:len(labels_test[i])]
        # convert labels from digit to bn
        output_dict_bn = predictions_transformation(
            labels_test[i], lbls_bn_pred, POS_test[i], tokens_test[i], map_label_bn,bn_to_wn_dict)
        predictions_sentences_bn = dict(
            list(predictions_sentences_bn.items()) + list(output_dict_bn.items()))
    # load mapping bn -> domains
    predictions_sentences_bn = load_dict(
        'predictions_sentences_bn', resources_path)
    bn_to_domains_dict = load_dict('bn_to_domains_dict', resources_path)

    # map fine-grained predictions to domains
    predictions_sentences_domain = dict()
    for elem in list(predictions_sentences_bn.keys()):
        bn = predictions_sentences_bn[elem]
        try:
            dom = bn_to_domains_dict[bn]
        except KeyError:
            dom = 'factotum'
        predictions_sentences_domain.update({elem: dom})
    # write on output file 
    with open(output_path, mode='w') as f:
        keys = list(predictions_sentences_domain.keys())
        values = list(predictions_sentences_domain.values())
        for i in range(len(keys)):
            if i < len(keys) - 1:
                f.write(str(keys[i]) + ' ' + str(values[i]) + '\n')
            else:
                f.write(str(keys[i]) + ' ' + str(values[i]))
        print(output_path, ' is written')


def predict_lexicographer(input_path: str, output_path: str, resources_path: str) -> None:
    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <lexicographerId>" format (e.g. "d000.s000.t000 noun.animal").

    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.

    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """
    # restore neural network model
    model = keras.models.load_model(resources_path + '/model--07.hd5f')

    # parse input file
    tokens_test, labels_test, POS_test, _, synsetmapping_test = xml_parsing(
        input_path)

    # load dictionaries from disk
    tokens_dict = load_dict('tokens', resources_path)
    pos_dict = load_dict('pos', resources_path)
    babelnet_labels_dict = load_dict('babelnet_labels', resources_path)
    lex_labels_dict = load_dict('lex_labels', resources_path)
    mask_senses = load_dict('mask_senses', resources_path)
    mask_lex = load_dict('mask_lex', resources_path)
    bn_to_lex_dict = load_dict('bn_to_lex_dict', resources_path)
    bn_to_wn_dict = load_dict('bn_to_wn_dict', resources_path)

    # restore hyperparameters
    BATCH_SIZE = 64
    SEQ_LEN = 80
    OUTPUT_VOCAB_SIZE = len(list(babelnet_labels_dict.keys()))
    LEX_OUTPUT_VOCAB_SIZE = len(list(lex_labels_dict.keys()))

    input_words_test = applyDict(tokens_test, tokens_dict)
    input_pos_test = applyDict(POS_test, pos_dict)

    X_test = keras.preprocessing.sequence.pad_sequences(
        input_words_test, maxlen=SEQ_LEN, padding='post')
    X_pos_test = keras.preprocessing.sequence.pad_sequences(
        input_pos_test, maxlen=SEQ_LEN, padding='post')

    # test data generator
    test_generator = dataGenerator(X_test, [], X_pos_test, [],
                                   OUTPUT_VOCAB_SIZE, LEX_OUTPUT_VOCAB_SIZE,
                                   BATCH_SIZE, mask_senses, mask_lex, False)

    STEPS_VAL = int(np.ceil(X_test.shape[0]/BATCH_SIZE))
    # compute predictions from model
    output = model.predict_generator(
        test_generator, steps=STEPS_VAL, verbose=True)
    pred_max_fine = np.argmax(output[0], axis=-1)
    pred_max_lex = np.argmax(output[1], axis=-1)
    map_label_bn = {v: k for k, v in babelnet_labels_dict.items()}
    map_label_lex = {v: k for k, v in lex_labels_dict.items()}
    
    predictions_sentences_bn = dict()
    predictions_sentences_lex = dict()
    for i in range(len(labels_test)):
        lbls_bn_pred = list(pred_max_fine[i])[0:len(labels_test[i])]
        lbls_lex_pred = list(pred_max_lex[i])[0:len(labels_test[i])]
        # convert labels from digit to bn
        output_dict_bn = predictions_transformation(
            labels_test[i], lbls_bn_pred, POS_test[i], tokens_test[i], map_label_bn,bn_to_wn_dict)
        predictions_sentences_bn = dict(
            list(predictions_sentences_bn.items()) + list(output_dict_bn.items()))
        # convert labels from digit to lex
        lex_values = set(list(bn_to_lex_dict.values()))
        output_dict_lex = predictions_lex(
            labels_test[i], lbls_lex_pred, lex_values, output_dict_bn, map_label_lex,bn_to_lex_dict)
        predictions_sentences_lex = dict(
            list(predictions_sentences_lex.items()) + list(output_dict_lex.items()))
    # write on output file
    with open(output_path, mode='w') as f:
        keys = list(predictions_sentences_lex.keys())
        values = list(predictions_sentences_lex.values())
        for i in range(len(keys)):
            if i < len(keys) - 1:
                f.write(str(keys[i]) + ' ' + str(values[i]) + '\n')
            else:
                f.write(str(keys[i]) + ' ' + str(values[i]))
        print(output_path, ' is written')
