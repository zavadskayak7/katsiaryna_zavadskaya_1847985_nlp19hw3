import xml.etree.ElementTree as eTree
from collections import deque
from collections import Counter
import pandas as pd
import numpy as np
import pickle
import h5py
import re
from nltk.corpus import wordnet as wn
import nltk
nltk.download('wordnet')
import random

def save_dict(dict, name, folder ):
    with open(folder + name + '.pkl', 'wb') as f:
        pickle.dump(dict, f, pickle.HIGHEST_PROTOCOL)

def load_dict(name, folder ):
    with open(folder + name + '.pkl', 'rb') as f:
        return pickle.load(f)

def OneSentenceParser(sentence, sentence_size_max=80, window=10):

    '''
    parsing one sentence per time
    '''

    lemmas = []
    ids = []
    POS = []
    lemmaslen = []
    synsetmapping_onesntc = []
    
    
    # for each word in a sentence keeps lemma and pos 
    for elem in sentence:
        lemma = elem.attrib['lemma'].lower()
          # skipps lemmas with zero len
        if len(lemma) < 1 or not lemma.isprintable():
            continue
        lemmas.append(lemma)
        pos = elem.attrib['pos']
        POS.append(pos)

        if elem.tag == "instance":
            ids.append(elem.attrib['id'])
            synsetmapping_onesntc.append((lemma, elem.attrib['id']))
        else:
            ids.append(lemma)
            synsetmapping_onesntc.append((lemma, lemma))

    # here we have all the lemmas for 1 sentence
    if len(lemmas) > sentence_size_max:
        lemmaslen.append(len(lemmas))
        lemmas_splt, ids_splt, POS_splt = SentenceSplitter(lemmas, POS, ids, sentence_size_max, window)

        return lemmas_splt, ids_splt, POS_splt, True, lemmaslen, synsetmapping_onesntc
    else:
        lemmaslen.append(len(lemmas))
        return lemmas, ids, POS, False, lemmaslen, synsetmapping_onesntc


def SentenceSplitter(lemmas, POS, ids, sentence_size_max, window):

    lemmas_splt = [] # list to save pieces of 1 sentence as different sentences
    POS_splt = []
    ids_splt = []

    for i in range(len(lemmas)):
        if ((i + 1) * sentence_size_max - i * window) > len(lemmas): 
            lemmas_splt.append(lemmas[i * sentence_size_max - i * window:])
            POS_splt.append(POS[i * sentence_size_max - i * window:])
            ids_splt.append(ids[i * sentence_size_max - i * window:])
            break
        else:
            b = i * sentence_size_max - i * window
            e = (i + 1) * sentence_size_max - i * window
            lemmas_splt.append(lemmas[b:e])
            POS_splt.append(POS[b:e])
            ids_splt.append(ids[b:e])
    return lemmas_splt, ids_splt, POS_splt

def xml_parsing(dataset_file):


    root = eTree.parse(dataset_file).getroot()
    tokens = deque()
    labels = deque()
    POS = deque()
    lemmasleN = []
    synsetmapping = {'<PAD>' : [],'<UNK>' : []}

    sentences = root.findall('.//sentence')
    for elem in sentences:
        terms, senses, pos, flag, lemmaslen, synsetmapping_onesntc  = OneSentenceParser(elem)
        
        
        if flag: # sentence was splitted, then intead of one list [] i have [[],[]] and add it to existing list
            tokens += terms
            labels += senses
            POS += pos
            lemmasleN+=lemmaslen
            for el in synsetmapping_onesntc:
                if el[0] not in list(synsetmapping.keys()):
                    synsetmapping.update({el[0]:[el[1]]})
                else:
                    nl = list(synsetmapping[el[0]])
                   
                    nl = list(set(nl + [el[1]]))
                    
                    synsetmapping.update({el[0]:nl})
               
        elif len(terms) > 0:
            tokens.append(terms)
            labels.append(senses)
            POS.append(pos)
            lemmasleN+=lemmaslen
            for el in synsetmapping_onesntc:
                if el[0] not in list(synsetmapping.keys()):
                    synsetmapping.update({el[0]:[el[1]]})
                else:
                    nl = list(synsetmapping[el[0]])
                    nl = list(set(nl + [el[1]]))
                    
                    synsetmapping.update({el[0]:nl})
        
    return tokens, labels, POS, lemmasleN, synsetmapping


def saveDataset(words, labels, pos, synMapping, name="semcor"):
  with open("words_" + name + ".pickle", 'wb') as filehandle:
    pickle.dump(words, filehandle)
  with open("labels_" + name + ".pickle", 'wb') as filehandle:
    pickle.dump(labels, filehandle)
  with open("pos_" + name + ".pickle", 'wb') as filehandle:
    pickle.dump(pos, filehandle)
  with open("synsetMapping_" + name + ".pickle", 'wb') as filehandle:
    pickle.dump(synMapping, filehandle, protocol=pickle.HIGHEST_PROTOCOL)

  return

def loadDataset(name="semcor"):
  with open("words_" + name + ".pickle", 'rb') as filehandle:
    words = pickle.load(filehandle)
  with open("labels_" + name + ".pickle", 'rb') as filehandle:
    labels = pickle.load(filehandle)
  with open("pos_" + name + ".pickle", 'rb') as filehandle:
    pos=pickle.load(filehandle)
  with open("synsetMapping_" + name + ".pickle", 'rb') as filehandle:
    synMapping = pickle.load(filehandle)
  
  return words, labels, pos, synMapping

#@brief create suitable dictionary from list of list
def createDict(list_of_lists):
    set_ = set()
    for elem in list_of_lists:
        set_.update(elem)

    keys = ['<PAD>','<UNK>'] + list(set_)
    values = range(len(keys))
    dict_ = dict(zip(keys, values))
    
    return dict_

def keyToStuff(labels, tokens, dictToStuff, use_words=True):
    returned = []
    for sentence, label in zip(tokens,labels):
        s = []
        for word,part in zip(sentence,shit):
            try:
                element = dictToStuff[part]
            except KeyError:
              if use_words:
                element = word
              else:
                continue
                  
            s.append(element)
                    
        returned.append(s)
    return returned

#@brief apply dictionary to list of list
def applyDict(list_of_lists, dict_, UNK = 1):
    returned = []
    for list_ in list_of_lists:
        returned_list = []
        for elem in list_:
            returned_list.append(dict_.get(elem, UNK))
        returned.append(returned_list)
    return returned

#@brief generate mask for both fine-grained and course-grained
#       the mask have -inf in the position of the label which should be 
#       not considered in the possible predictions of a world and 0 in the others
#       this mask will be summed in the logits of the corresponding layer (fine/course)
def makeFineAndCourseGrainedMask(mapping, wordDict, bn_to_lex_dict, babelnet_labels_dict, lex_labels_dict):
  mask_fine_grained = dict()
  mask_course_grained = dict()
  for k,v in mapping.items(): # {lemma:semcorid}
    word_idx = wordDict.get(k,1)
    mask = np.ones((OUTPUT_VOCAB_SIZE)) * -1e15
    mask_course = np.ones((LEX_OUTPUT_VOCAB_SIZE)) * -1e15
    b_i = -1
    for s_i in v:
      # the try except are used to handle unknown senses
      try:
        b_i = semcor_to_bn_dict[s_i]
        sense_idx = babelnet_labels_dict[b_i] 
        mask[sense_idx] = 0
      except KeyError:
        idx = babelnet_labels_dict.get(k,1)
        mask[idx] = 0
      try:
        lex = bn_to_lex_dict[b_i]
        lex_idx = lex_labels_dict[lex]
        mask_course[lex_idx] = 0
      except KeyError:
        idx = lex_labels_dict.get(k,1)
        mask_course[idx] = 0
        
        
    mask_fine_grained[word_idx] = mask
    mask_course_grained[word_idx] = mask_course
  return mask_fine_grained, mask_course_grained


#@brief data generator for the keras model
# this function will generate batches for words, pos and masks 
# further if train=True also the course-grained and fine-grained labels batches will be returned
def dataGenerator(X, Y, POS, Y2, SIZE, SIZE2, BATCH_SIZE, dict_word_senses, dict_word_lex, train=True):
  while True:
    # shuffle the data at each epoch if train
    if train:
      Dataset = list(zip(X, Y, POS, Y2))
      random.shuffle(Dataset)
      X, Y, POS, Y2 = zip(*Dataset)
      num_batches = int(np.ceil(len(X)/ BATCH_SIZE))
    else:
      num_batches = int(np.ceil(len(X)/ BATCH_SIZE))
   
    # extract the batches from quantities   
    for j in range(num_batches):
        if (j+1) * BATCH_SIZE > len(X):
            sequences = X[j*BATCH_SIZE:]
            pos = POS[j*BATCH_SIZE:]
            if train:
              labels = Y[j*BATCH_SIZE:]
              aux_labels = Y2[j*BATCH_SIZE:]
        
        else:
            sequences = X[j*BATCH_SIZE:(j+1) * BATCH_SIZE]
            pos = POS[j*BATCH_SIZE:(j+1) * BATCH_SIZE]
            if train:
              labels = Y[j*BATCH_SIZE:(j+1) * BATCH_SIZE]
              aux_labels = Y2[j*BATCH_SIZE:(j+1) * BATCH_SIZE]
        # create dummy labels in the case of testing
        if not train:
          labels = sequences
          aux_labels = sequences
          
        batch_lengths = np.array([len(x) for x in sequences])
        max_length_sequences = np.max(batch_lengths)
        
        # batches have to be returned as numpy arrays
        batch_x = np.zeros((len(sequences),max_length_sequences),dtype=np.int32)
        batch_pos = np.zeros((len(pos),max_length_sequences),dtype=np.int32)
        batch_masks_fine = np.zeros((len(sequences), max_length_sequences, SIZE), dtype=np.float32)
        batch_masks_course = np.zeros((len(sequences), max_length_sequences, SIZE2), dtype=np.float32)
        if train:
          batch_y_main = np.zeros((len(labels),max_length_sequences),dtype=np.int32)
          batch_y_aux = np.zeros((len(aux_labels),max_length_sequences),dtype=np.int32)

        for idx, seq in enumerate(zip(sequences,labels,pos, aux_labels)):
            batch_x[idx][:len(seq[0])] = seq[0]
            batch_pos[idx][:len(seq[2])] = seq[2]
            if train:
              batch_y_main[idx][:len(seq[1])] = seq[1]
              batch_y_aux[idx][:len(seq[1])] = seq[3]
            # generate the masks using the dictionaries 
            for j, w in enumerate(seq[0]):
                batch_masks_fine[idx][j] = dict_word_senses.get(w, np.full((SIZE),-1e15))
                batch_masks_course[idx][j] = dict_word_lex.get(w, np.full((SIZE2),-1e15))
        
        if train:
          output = {'finegrained' : keras.utils.to_categorical(batch_y_main, num_classes=SIZE), 
                    'coursegrained' : keras.utils.to_categorical(batch_y_aux, num_classes=SIZE2)}
          yield ([batch_x, batch_pos, batch_masks_fine, batch_masks_course], output)
        else:
          yield ([batch_x, batch_pos, batch_masks_fine, batch_masks_course])

#@brief compute MFS from wordnet
def WordPos_MFS(word,pos,bn_to_wn_dict):
  p = pos.lower()[0]
  synsets = wn.synsets(word)
  mfs = '<UNK>'

  for sense in synsets:
    if sense.pos() == p:
      synset_id = "wn:" + str(sense.offset()).zfill( 8) + sense.pos()
      mfs = bn_to_wn_dict[synset_id]
      break
    else:
      synset_id = "wn:" + str(synsets[0].offset()).zfill( 8) + synsets[0].pos()
      mfs = bn_to_wn_dict[synset_id]
 
  return mfs

#@brief utilities to fine grained predictions (select between network output and MFS)
def predictions_transformation(keys,labels_bn,pos,tokens, map_label_bn,bn_to_wn_dict):
  output_id = list()
  output_bn = list()
  for i in range(len(keys)):
    if re.findall("d....s....t...", keys[i]):
      bn = map_label_bn[labels_bn[i]]
      if not re.findall("^bn:", bn):
        bn = WordPos_MFS(tokens[i],pos[i],bn_to_wn_dict)
      output_id.append(keys[i])
      output_bn.append(bn)
    else:
      continue
  output_dict = dict(zip(output_id, output_bn))
  return output_dict

#@brief utilities for course grained prediction (select between network and mappings)
def predictions_lex(keys,labels_lex,lex_values,output_dict_bn, map_label_lex,bn_to_lex_dict):
  output_id = list()
  output_lex = list()
  for i in range(len(keys)):
    if re.findall("d....s....t...", keys[i]):
      lex = map_label_lex[labels_lex[i]]
      if lex not in lex_values:
        bn = output_dict_bn[keys[i]]
        lex = bn_to_lex_dict[bn]
      output_id.append(keys[i])
      output_lex.append(lex)
    else:
      continue
  output_dict_lex = dict(zip(output_id, output_lex))
  return output_dict_lex
  