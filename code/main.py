from utils import *
# relative path to resources (dict files) and input files (semcor e semeval)
resources_path = "../resources/"
input_path = "../datasets/"


# parse dataset and save the dataset to avoid massive lose of time at each run
# (i keep decommented the parsing to keep the code clean)

tokens, labels, POS, lemmaslen, synsetmapping = xml_parsing(input_path + 'semcor.data.xml')
saveDataset(tokens, labels, POS, synsetmapping)
tokens_val, labels_val, POS_val, _, synsetmapping_val = xml_parsing(input_path + 'semeval2007.data.xml')
saveDataset(tokens_val, labels_val, POS_val, synsetmapping_val, name="semeval07")

# load useful dictionary from resources and save each of them
semeval07dict = {}
with open(resources_path + 'semeval2007.gold.key.txt', mode='r') as f:
    for row in f:
        semeval07dict.update({row.split()[0]: row.split()[1]})

semcordict = {}
with open(resources_path + 'semcor.gold.key.txt', mode='r') as f:
    for row in f:
        semcordict.update({row.split()[0]: row.split()[1]})

bn_to_wn_dict = {}
with open(resources_path + 'babelnet2wordnet.tsv', mode='r') as f:
    for row in f:
        bn_to_wn_dict.update({row.split()[1]: row.split()[0]})

save_dict(bn_to_wn_dict,"bn_to_wn_dict", resources_path)

bn_to_domains_dict = {}
with open( resources_path + 'babelnet2wndomains.tsv', mode='r') as f:
    for row in f:
        bn_to_domains_dict.update({row.split()[0]: ' '.join(row.split()[1:])})

save_dict(bn_to_domains_dict,"bn_to_domains_dict", resources_path)

bn_to_lex_dict = {}
with open(resources_path + 'babelnet2lexnames.tsv', mode='r') as f:
    for row in f:
        bn_to_lex_dict.update({row.split()[0]: row.split()[1]})

save_dict(bn_to_lex_dict,"bn_to_lex_dict", resources_path)

# transform semcor/semeval ids to babelnet synsets
semcor_to_bn_dict = {}
for elem in semcordict.keys():
    synset = wn.lemma_from_key(semcordict[elem]).synset()
    synset_id = "wn:" + str(synset.offset()).zfill( 8) + synset.pos()
    semcor_to_bn_dict.update({elem:bn_to_wn_dict[synset_id]})

semeval07_to_bn_dict = {}
for elem in semeval07dict.keys():
    synset = wn.lemma_from_key(semeval07dict[elem]).synset()
    synset_id = "wn:" + str(synset.offset()).zfill( 8) + synset.pos()
    semeval07_to_bn_dict.update({elem:bn_to_wn_dict[synset_id]})


# create target labels for fine-grained, course-grained and domains
labels_bn = keyToStuff(labels,tokens,semcor_to_bn_dict)
labels_lex = keyToStuff(labels_bn,tokens,bn_to_lex_dict) 
labels_domain = keyToStuff(labels_bn,tokens,bn_to_domains_dict)
# same for validation
labels_bn_val = keyToStuff(labels_val,tokens_val,semeval07_to_bn_dict)
labels_lex_val = keyToStuff(labels_bn_val,tokens_val, bn_to_lex_dict)

#########################################################################

# subsample the possible fine grained labels
# the threshold of 10000 was choosen in order to include just the senses that appear 
# at least 5 times in the dataset

VOCAB_SIZE = 10000
total_list = []
for s in labels_bn:
    for w in s:
        total_list.append(w)
c = Counter(total_list)

subset_dict_labels = {}
for k,v in c.most_common(VOCAB_SIZE):
    subset_dict_labels[k] = k

# transform the labels not included in the subsampling
# in <UNK>    
labels_bn = applyDict(labels_bn, subset_dict_labels)

# subsample the possible course-grained labels

VOCAB_SIZE = 5000
total_list = []
for s in labels_lex:
    for w in s:
        total_list.append(w)
c = Counter(total_list)

subset_dict_labels = {}
for k,v in c.most_common(VOCAB_SIZE):
    subset_dict_labels[k] = k

# transform the labels not included in the subsampling            
labels_lex = applyDict(labels_lex, subset_dict_labels)

# create dicts from data
tokens_dict = createDict(tokens)
pos_dict = createDict(POS)
babelnet_labels_dict = createDict(labels_bn)
lex_labels_dict = createDict(labels_lex)

# save dicts for testing
save_dict(tokens_dict,"tokens", resources_path)
save_dict(pos_dict,"pos", resources_path)
save_dict(babelnet_labels_dict,"babelnet_labels", resources_path)
save_dict(lex_labels_dict,"lex_labels", resources_path)

# apply dictionary to the datasets (both train and validation)
input_words = applyDict(tokens,tokens_dict)
input_pos = applyDict(POS,pos_dict)
output_bn = applyDict(labels_bn,babelnet_labels_dict)
output_lex = applyDict(labels_lex,lex_labels_dict)

input_words_val = applyDict(tokens_val, tokens_dict)
input_pos_val = applyDict(POS_val, pos_dict)
output_bn_val = applyDict(labels_bn_val, babelnet_labels_dict)
output_lex_val = applyDict(labels_lex_val,lex_labels_dict)

# store dimension of the dictionary to construct the model
INPUT_VOCAB_SIZE = len(list(tokens_dict.keys()))
POS_VOCAB_SIZE = len(list(pos_dict.keys()))
OUTPUT_VOCAB_SIZE = len(list(babelnet_labels_dict.keys()))
LEX_OUTPUT_VOCAB_SIZE = len(list(lex_labels_dict.keys()))

# construct fine grained and course grained mask
mask_senses, mask_lex = makeFineAndCourseGrainedMask(synsetmapping, tokens_dict,
                                                     bn_to_lex_dict, babelnet_labels_dict,
                                                     lex_labels_dict)


save_dict(mask_senses,"mask_senses", resources_path)
save_dict(mask_lex,"mask_lex", resources_path)

from keras.layers import Embedding
from keras.models import Model
import keras

######################################### F1 score metric callback ############################

# @brief this class compute the f1 score for both the fine grained and the course grained
#        i had to implement this class because the fit_generator method of the keras model
#        don't include a suitable f1 measure for the course-grained/fine-grained predictions

class Metrics(keras.callbacks.Callback):

  # @brief Constructor : save dict and reverse dict 
  # which are use to understand which prediction is a sense/lex

  def __init__(self, dict_, dictlex_):
    self.sense_dict = dict_
    self.reverse_sense_dict = {v: k for k,v in dict_.items()}
    self.lex_dict = dictlex_
    self.reverse_lex_dict = {v: k for k,v in dictlex_.items()}

  # @brief set generators  
  def setData(self, validation_generator, validation_steps):
    self.validation_generator = validation_generator
    self.validation_steps = validation_steps
  
  def on_train_begin(self, logs={}):
    self.val_f1s = []
    self.val_recalls = []
    self.val_precisions = []
  
  # @brief at the end of each epoch evaluate the model on validation set
  def on_epoch_end(self, epoch, logs={}):
    for j in range(self.validation_steps):
      x,y = next(self.validation_generator)
      # extract predictions
      predictions = self.model.predict(x)
      # get the finegrained predictions
      val_predict = np.asarray(predictions[0]) 
      val_targ = y['finegrained']

      # compute prediction and recall as:
      #   precision = num_corrected_classified / num_predicted_senses
      #   recall    = num_corrected_classified / num_true_senses
      cc = 0
      total_senses = 0
      total_pred = 0
      for y_, y_pred in zip(val_targ, val_predict):
        for y_t, y_pred_t in zip(y_,y_pred):
          label = np.argmax(y_t)
          word_label = self.reverse_sense_dict[label]
          answer_given = False
          # check if the current label is a sense
          if "bn:" in word_label:
            total_senses += 1
            sorted_predictions = np.sort(y_pred_t)[::-1]
            # get the top prediction
            top_3_pred = sorted_predictions[:1]
            for prop_y_i in top_3_pred:
              pred_idx = list(y_pred_t).index(prop_y_i)
              word_pred = self.reverse_sense_dict[pred_idx]
              # check if the prediction is a sense
              if "bn:" in word_pred and not answer_given:
                total_pred+=1
                answer_given = True
              # count the correctly classified samples
              if pred_idx == label:
                cc += 1
              
      # same computations for the course-grained predictions
      cc_lex = 0
      total_lex = 0
      total_lex_pred = 0
      val_predict_lex = np.asarray(predictions[1])
      val_targ_lex = y['coursegrained']
      
      for y_lex, y_pred_lex in zip(val_targ_lex, val_predict_lex):
        for y_t_lex, y_pred_t_lex in zip(y_lex,y_pred_lex):
          label_lex = np.argmax(y_t_lex)
          word_label_lex = self.reverse_lex_dict[label_lex] 
          answer_given_lex = False
          # check if the current label is a lex
          if "noun." in word_label_lex or "verb." in word_label_lex or\
          "adj." in word_label_lex or "adv." in word_label_lex:
            total_lex += 1
            sorted_predictions_lex = np.sort(y_pred_t_lex)[::-1]
            top_1_pred_lex = sorted_predictions_lex[:1]
            for prop_y_i_lex in top_1_pred_lex:
              pred_idx_lex = list(y_pred_t_lex).index(prop_y_i_lex)
              word_pred_lex = self.reverse_lex_dict[pred_idx_lex]
              # check if the current prediction is a lex
              if "noun." in word_pred_lex or "verb." in word_pred_lex or\
              "adj." in word_pred_lex or "adv." in word_pred_lex:
                total_lex_pred+=1
                answer_given_lex = True
                
              if pred_idx_lex == label_lex:
                cc_lex += 1
                break
              y_pred_t_lex[pred_idx_lex] = 0

    # compute the f1-score for both the fine-grained and course-grained            
    precision_lex = cc_lex/total_lex_pred
    recall_lex = cc_lex/total_lex
    f1_score_lex = 2 * precision_lex * recall_lex / (precision_lex + recall_lex + 1e-8)
    precision = cc/total_pred
    recall = cc/total_senses
    f1_score = 2 * precision * recall / (precision + recall + 1e-8)
    print("Precision : {} -- Recall : {} -- F1 Score : {}".format(precision, recall, f1_score))
    print("Precision_lex : {} -- Recall_lex : {} -- F1 Score_lex : {}".format(precision_lex, recall_lex, f1_score_lex))
    return
################################################################################################################
                                                    # MODEL
################################################################################################################

# set the hyperparameters for the network
SEQ_LEN = 80
EMBEDDINGS_SIZE_WORDS = 300
HIDDEN_UNITS_1 = 512
HIDDEN_UNITS_2 = 512
EMBEDDINGS_SIZE_POS = 16
BATCH_SIZE = 16
EPOCH = 10

# pad the sentences and the labels 
X_tokens = keras.preprocessing.sequence.pad_sequences(input_words, maxlen = SEQ_LEN, padding='post')
X_pos = keras.preprocessing.sequence.pad_sequences(input_pos, maxlen = SEQ_LEN, padding='post')

Y_bn = keras.preprocessing.sequence.pad_sequences(output_bn, maxlen = SEQ_LEN, padding='post')
Y_lex = keras.preprocessing.sequence.pad_sequences(output_lex, maxlen = SEQ_LEN, padding='post')

X_val = keras.preprocessing.sequence.pad_sequences(input_words_val, maxlen = SEQ_LEN, padding='post')
X_pos_val = keras.preprocessing.sequence.pad_sequences(input_pos_val, maxlen = SEQ_LEN, padding='post')
Y_val = keras.preprocessing.sequence.pad_sequences(output_bn_val, maxlen = SEQ_LEN, padding='post')
Y_lex_val = keras.preprocessing.sequence.pad_sequences(output_lex_val, maxlen = SEQ_LEN, padding='post')

# construct the inputs
words = keras.layers.Input(shape=(SEQ_LEN,), name="words", dtype='int32')
pos = keras.layers.Input(shape=(SEQ_LEN,), name="pos", dtype='int32')

# create embeddings for the words
embedding_tokens = keras.layers.Embedding(input_dim=INPUT_VOCAB_SIZE,
                                          output_dim=EMBEDDINGS_SIZE_WORDS,
                                          mask_zero=True,
                                          input_length = SEQ_LEN,
                                          name="embeddings")(words)
# create embeddings for poses
embedding_pos = keras.layers.Embedding(input_dim=POS_VOCAB_SIZE, output_dim=EMBEDDINGS_SIZE_POS, mask_zero=True, input_length = SEQ_LEN)(pos)

embedding = keras.layers.concatenate([embedding_tokens, embedding_pos], axis=-1)

bi_lstm1 = keras.layers.Bidirectional(keras.layers.LSTM(HIDDEN_UNITS_1, return_sequences=True),
                                      merge_mode='concat')(embedding)

sense_mask = keras.layers.Input(shape=(SEQ_LEN, OUTPUT_VOCAB_SIZE), name="sense_mask")

lex_mask = keras.layers.Input(shape=(SEQ_LEN, LEX_OUTPUT_VOCAB_SIZE), name="lex_mask")

lex_logits = keras.layers.TimeDistributed(keras.layers.Dense(LEX_OUTPUT_VOCAB_SIZE,
                                                                 activation='linear'))(bi_lstm1)
# apply lex mask
masked_lex_logits = keras.layers.Add()([lex_mask, lex_logits])

# generate predictions fro the course-graine senses at the first bi-lstm layer
lex_prediction = keras.layers.Softmax(axis=-1, name='coursegrained')(masked_lex_logits)

bi_lstm2 = keras.layers.Bidirectional(keras.layers.LSTM(HIDDEN_UNITS_2,return_sequences=True),
                                      merge_mode='concat')(bi_lstm1)

logits = keras.layers.TimeDistributed(keras.layers.Dense(OUTPUT_VOCAB_SIZE, activation='linear'))(bi_lstm2)

masked_logits = keras.layers.Add()([sense_mask, logits])

# generate prediction for fine-grained senses
prediction = keras.layers.Softmax(axis=-1, name='finegrained')(masked_logits)

model = Model(inputs=[words, pos, sense_mask, lex_mask], outputs=[prediction, lex_prediction])

optimizer = keras.optimizers.Adadelta(lr=1.0)

# compile the model
model.compile(loss='categorical_crossentropy', \
              loss_weights = {'finegrained' : 1.0, 'coursegrained' : 1.0}, \
              optimizer=optimizer, metrics=['categorical_accuracy'])

# generate callbacks
metrics = Metrics(babelnet_labels_dict,lex_labels_dict)

model_saver = keras.callbacks.ModelCheckpoint(filepath="model--{epoch:02d}.hd5f",
                                              monitor='train_loss')

tensorboard = keras.callbacks.TensorBoard(log_dir='./logs')

STEPS = int(np.ceil(X_tokens.shape[0]/BATCH_SIZE))
STEPS_VAL = int(np.ceil(X_val.shape[0]/BATCH_SIZE))

# instanciate the data-generator for training and testing     
train_generator = dataGenerator(X_tokens, Y_bn, X_pos, Y_lex, 
                                OUTPUT_VOCAB_SIZE, LEX_OUTPUT_VOCAB_SIZE,
                                BATCH_SIZE,mask_senses, mask_lex, True)
validation_generator = dataGenerator(X_val, Y_val, X_pos_val, Y_lex_val,
                                     OUTPUT_VOCAB_SIZE, LEX_OUTPUT_VOCAB_SIZE,
                                     BATCH_SIZE, mask_senses, mask_lex, True)

metrics.setData(validation_generator, STEPS_VAL)
# run training
model.fit_generator(train_generator,
                    steps_per_epoch=STEPS,
                    epochs=EPOCH,
                    callbacks=[metrics, model_saver, tensorboard])
